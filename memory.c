/* Author(s): Akshaya Uttamadoss
              Christina Funk 

 * COS 318, Fall 2013: Project 5 Virtual Memory
 * Implementation of the memory manager for the kernel.
*/

/* memory.c
 *
 * Note: 
 * There is no separate swap area. When a data page is swapped out, 
 * it is stored in the location it was loaded from in the process' 
 * image. This means it's impossible to start two processes from the 
 * same image without screwing up the running. It also means the 
 * disk image is read once. And that we cannot use the program disk.
 *
 */

#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "memory.h"
#include "thread.h"
#include "util.h"
#include "interrupt.h"
#include "tlb.h"
#include "usb/scsi.h"

/* Static global variables */
// Keep track of all pages: their vaddr, status, and other properties
static page_map_entry_t page_map[PAGEABLE_PAGES];

// address of the kernel page directory (shared by all kernel threads)
static uint32_t *kernel_pdir;

// allocate the kernel page tables
static uint32_t *kernel_ptabs[N_KERNEL_PTS];

//other global variables...
static uint32_t page_count = 0;

static lock_t lock;
 
/* Main API */

/* Use virtual address to get index in page directory. */
uint32_t get_dir_idx(uint32_t vaddr){
  return (vaddr & PAGE_DIRECTORY_MASK) >> PAGE_DIRECTORY_BITS;
}

/* Use virtual address to get index in a page table. */
uint32_t get_tab_idx(uint32_t vaddr){
  return (vaddr & PAGE_TABLE_MASK) >> PAGE_TABLE_BITS;
}

/* TODO: Returns physical address of page number i */
uint32_t *page_addr(int i){
  return (uint32_t *) (MEM_START + PAGE_SIZE * i); 
}

/* Set flags in a page table entry to 'mode' */
void set_ptab_entry_flags(uint32_t * pdir, uint32_t vaddr, uint32_t mode){
  uint32_t dir_idx = get_dir_idx((uint32_t) vaddr);
  uint32_t tab_idx = get_tab_idx((uint32_t) vaddr);
  uint32_t dir_entry;
  uint32_t *tab;
  uint32_t entry;
  
  pdir = (uint32_t *)((uint32_t)pdir & PE_BASE_ADDR_MASK);
  
  dir_entry = pdir[dir_idx];
  ASSERT(dir_entry & PE_P); /* dir entry present */
  tab = (uint32_t *) (dir_entry & PE_BASE_ADDR_MASK);
  /* clear table[index] bits 11..0 */
  entry = tab[tab_idx] & PE_BASE_ADDR_MASK;

  /* set table[index] bits 11..0 */
  entry |= mode & ~PE_BASE_ADDR_MASK;
  tab[tab_idx] = entry;

  /* Flush TLB because we just changed a page table entry in memory */
  flush_tlb_entry(vaddr);
}

/* Initialize a page table entry
 *  
 * 'vaddr' is the virtual address which is mapped to the physical
 * address 'paddr'. 'mode' sets bit [12..0] in the page table entry.
 *   
 * If user is nonzero, the page is mapped as accessible from a user
 * application.
 */
void init_ptab_entry(uint32_t * table, uint32_t vaddr,
         uint32_t paddr, uint32_t mode){
  int index = get_tab_idx(vaddr);
  table = (uint32_t *)((uint32_t)table & PE_BASE_ADDR_MASK);
  table[index] =
    (paddr & PE_BASE_ADDR_MASK) | (mode & ~PE_BASE_ADDR_MASK);
  flush_tlb_entry(vaddr);
}

/* Insert a page table entry into the page directory. 
 *   
 * 'mode' sets bit [12..0] in the page table entry.
 */
void insert_ptab_dir(uint32_t * dir, uint32_t *tab, uint32_t vaddr, 
		       uint32_t mode){

  uint32_t access = mode & MODE_MASK;
  int idx = get_dir_idx(vaddr);

  dir[idx] = ((uint32_t)tab & PE_BASE_ADDR_MASK) | access;
}

/* TODO: Allocate a page. Return page index in the page_map directory.
 * 
 * Marks page as pinned if pinned == TRUE. 
 * Swap out a page if no space is available. 
 */
int page_alloc(int pinned){
  int ind = -1;
  int i;

  for (i = 0; i < PAGEABLE_PAGES; i++){
    if(page_map[i].free == 1) {
      ind = i;
      break;
    }
  }
  if (ind == -1) {
    ind = page_replacement_policy();
    page_swap_out(ind);      
  }
  
  page_map[ind].free = 0; 
  page_map[ind].pinned = pinned; 
  bzero ((char *)page_addr(ind), PAGE_SIZE);
  return ind;
}

/* TODO: Set up kernel memory for kernel threads to run.
 *
 * This method is only called once by _start() in kernel.c, and is only 
 * supposed to set up the page directory and page tables for the kernel.
 */
void init_memory(void){
  int i, j;

  // set physical addresses of page_map objects
  for (i = 0; i < PAGEABLE_PAGES; i++) { 
    uint32_t addr = MEM_START + PAGE_SIZE * i; 
    if (addr >= MAX_PHYSICAL_MEMORY) {
      break;
    }
    page_map[i].free = 1;
  }

  lock_init(&lock); 

  //allocate kernel page directory
  int dir = page_alloc(TRUE);
  page_map[dir].vaddr = page_addr(dir);
  kernel_pdir = page_addr(dir);
  page_map[i].count = page_count; 
  page_count++; 

  //allocate kernel page tables 
  for(i = 0; i < N_KERNEL_PTS; i++) {
    int tab = page_alloc(TRUE); 
    kernel_ptabs[i] = page_addr(tab); 
    page_map[tab].vaddr = page_addr(tab);
    page_map[i].count = page_count; 
    page_count++; 
    insert_ptab_dir(kernel_pdir, kernel_ptabs[i], (i << PAGE_DIRECTORY_BITS), PE_P | PE_RW ); 
    // initialze kernel page table entries 
    for (j = 0; j < PAGE_N_ENTRIES; j++) {
      int addr = (j << PAGE_TABLE_BITS) | (i << PAGE_DIRECTORY_BITS); 
      init_ptab_entry(kernel_ptabs[i], addr, addr, PE_P | PE_RW);
    }
  }

  set_ptab_entry_flags(kernel_pdir, SCREEN_ADDR, PE_RW | PE_P | PE_US); 
}


/* TODO: Set up a page directory and page table for a new 
 * user process or thread. */
void setup_page_table(pcb_t * p){
  
  int k, l, i;

  //only assign page directory if kerenel thread
  if (p->is_thread) {
    p->page_directory = kernel_pdir;
    return;
  }
  
  lock_acquire(&lock); 
 
  // Page directory for process
  i = page_alloc(TRUE); 
  p->page_directory = page_addr(i);
  page_map[i].vaddr = PROCESS_START;
  page_map[i].swap_loc = p->swap_loc;
  page_map[i].swap_size = p->swap_size;
  page_map[i].count = page_count; 
  page_map[i].pdir = p->page_directory;
  page_count++; 

  // Regular page table
  int tab = page_alloc(TRUE); 
  uint32_t *tab_addr = page_addr(tab); 
  insert_ptab_dir(p->page_directory, tab_addr, PROCESS_START, PE_P | PE_RW | PE_US);
  page_map[tab].vaddr = PROCESS_START;
  page_map[tab].swap_loc = p->swap_loc;
  page_map[tab].swap_size = p->swap_size;
  page_map[tab].count = page_count;
  page_map[tab].pdir = p->page_directory;
  page_count++;

  //map kernel page tables into process directory 
  for(k = 0; k < N_KERNEL_PTS; k++) {
    insert_ptab_dir(p->page_directory, kernel_ptabs[k], (k << PAGE_DIRECTORY_BITS), PE_P | PE_RW | PE_US); 
  }

  // Stack page table
  int tab_stack = page_alloc(TRUE);
  uint32_t *tab_addr_stack = page_addr(tab_stack);
  insert_ptab_dir(p->page_directory, tab_addr_stack, PROCESS_STACK, PE_P | PE_RW | PE_US);
  page_map[tab_stack].vaddr = PROCESS_STACK;
  page_map[tab_stack].swap_loc = p->swap_loc;
  page_map[tab_stack].swap_size = p->swap_size;
  page_map[tab_stack].count = page_count;
  page_map[tab_stack].pdir = p->page_directory;
  page_count++; 


  // Allocate the stack pages
  for (l = 0; l < N_PROCESS_STACK_PAGES; l++) {
    int ind = page_alloc(TRUE);
    uint32_t * addr_stack = page_addr(ind); 

    page_map[ind].vaddr = PROCESS_STACK - l * PAGE_SIZE;
    page_map[ind].swap_loc = p->swap_loc;
    page_map[ind].swap_size = p->swap_size;
    page_map[ind].count = page_count;
    page_map[ind].pdir = p->page_directory;
    page_count++;

    init_ptab_entry(tab_addr_stack, PROCESS_STACK - l * PAGE_SIZE, addr_stack,  PE_P | PE_RW | PE_US);
  }

  lock_release(&lock); 
}

/* TODO: Swap into a free page upon a page fault.
 * This method is called from interrupt.c: exception_14(). 
 * Should handle demand paging.
 */
void page_fault_handler(void){
  
  lock_acquire(&lock);
  
  // Find the faulting page  
  uint32_t vaddr = current_running->fault_addr; //virtual address
  
  // Allocate a page of physical memory
  int i = page_alloc(FALSE);

  // Set page metadata 
  page_map[i].swap_loc = current_running->swap_loc;
  page_map[i].swap_size = current_running->swap_size;
  page_map[i].vaddr = vaddr;
  page_map[i].pdir = current_running->page_directory;

  // Load the contents of the page
  page_swap_in(i); 

  // Update the page table of the process
  uint32_t dir = get_dir_idx(vaddr);
  init_ptab_entry(current_running->page_directory[dir], vaddr, page_addr(i), PE_P | PE_RW | PE_US); 

  lock_release(&lock);
}

/* Get the sector number on disk of a process image
 * Used for page swapping. */
 int get_disk_sector(page_map_entry_t * page){
  return page->swap_loc +
    ((page->vaddr - PROCESS_START) / PAGE_SIZE) * SECTORS_PER_PAGE;
 }

/* TODO: Swap i-th page in from disk (i.e. the image file) */
/* Is i-th related to the page_map? or on disk? How do we know how big the page is on disk ?*/ 
void page_swap_in(int i){
  int disk_sector = get_disk_sector(&page_map[i]);

  //regular page 
  if ((disk_sector + SECTORS_PER_PAGE) <= (page_map[i].swap_loc + page_map[i].swap_size))
    scsi_read(disk_sector, SECTORS_PER_PAGE, (char *)page_addr(i)); // read into
  
  //if last page on disk
  else {  
    int diff = (disk_sector + SECTORS_PER_PAGE) - (page_map[i].swap_loc + page_map[i].swap_size);
     scsi_read(disk_sector, SECTORS_PER_PAGE - diff, (char *)page_addr(i)); // read into
  }

  page_map[i].count = page_count; 
  page_count++; 
}

/* TODO: Swap i-th page out to disk.
 *   
 * Write the page back to the process image.
 * There is no separate swap space on the USB.
 * 
 */
void page_swap_out(int i){
  int disk_sector = get_disk_sector(&page_map[i]);
   
  // mark page table entry as not present
  set_ptab_entry_flags(page_map[i].pdir, page_map[i].vaddr, PE_RW | PE_US ); 

  //regular page 
  if ((disk_sector + SECTORS_PER_PAGE) <= (page_map[i].swap_loc + page_map[i].swap_size))
    scsi_write(disk_sector, SECTORS_PER_PAGE, (char *)page_addr(i)); // write to
  
  //if last page on disk
  else {  
    int diff = (disk_sector + SECTORS_PER_PAGE) - (page_map[i].swap_loc + page_map[i].swap_size);
    scsi_write(disk_sector, SECTORS_PER_PAGE - diff, (char *)page_addr(i)); // write into
  }
  
  page_map[i].free = 1; 
  page_map[i].pinned = 0; 
}

/* TODO: Decide which page to replace, return the page number  */
int page_replacement_policy(void){
  int ind = -1; 
  int i;
 
  for(i = 0; i < PAGEABLE_PAGES; i++) {
    if (page_map[i].pinned == 0) {
      if ((page_map[ind].count > page_map[i].count) || (ind == -1)) {
        ind = i;
      }
    }
  }
  
  return ind;  
}
